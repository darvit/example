Это библиотека для интеграции API с сервисом ponominalu.ru, используется в проекте платежной системы.

Для использования в конфиг файле приложения должно быть следующее:

```
#!xml

  <appSettings>
    <add key="ponominalu_api" value="http://api.cultserv.ru/jtransport/partner/"/>
    <add key="ponominalu_session" value="123"/>
  </appSettings>
```


Описание API - http://api.cultserv.ru/public/docs/