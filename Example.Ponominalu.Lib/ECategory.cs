﻿namespace Example.Ponominalu.Lib
{
	/// <summary>
	/// Список категорий мероприятий
	/// </summary>
	public enum ECategory
	{
		/// <summary>
		/// Рекомендуем
		/// </summary>
		Top = 94,
		/// <summary>
		/// Спорт
		/// </summary>
		Sport = 11,
		/// <summary>
		/// Фестивали
		/// </summary>
		Festivals = 54,
		/// <summary>
		/// Музем
		/// </summary>
		Museum = 89,
		/// <summary>
		/// Новогодние ёлки
		/// </summary>
		Christmas = 95,
		/// <summary>
		/// Семинары
		/// </summary>
		Seminars = 92,
		/// <summary>
		/// Экскурсии
		/// </summary>
		Excursions = 101,
		/// <summary>
		/// Детям
		/// </summary>
		Kids = 12,
		/// <summary>
		/// Концерты
		/// </summary>
		Concerts = 10,
		/// <summary>
		/// Выставки
		/// </summary>
		Exhibitions = 90,
		/// <summary>
		/// Игры-квесты
		/// </summary>
		GameQuests = 104,
		/// <summary>
		/// Шоу
		/// </summary>
		Show = 8,
		/// <summary>
		/// Театр
		/// </summary>
		Theatre = 6
	}
}