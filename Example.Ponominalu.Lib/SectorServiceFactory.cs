﻿using System;

using Example.Ponominalu.Lib.Core.Services;

namespace Example.Ponominalu.Lib
{
	public sealed class SectorServiceFactory
	{
		/// <summary>
		/// Создание сервиса секторов
		/// </summary>
		/// <param name="eventId">ID события</param>
		/// <returns>Сервис секторов</returns>
		public static ISectorService Create(int eventId)
		{
			return Activator.CreateInstance(typeof(PonominaluSectorService), eventId) as ISectorService;
		}
	}
}