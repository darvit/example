﻿using System;
using System.Runtime.Serialization;

namespace Example.Ponominalu.Lib
{
	[Serializable]
	public sealed class PonominaluApiException : Exception
	{
		private const string CODE = "api_code";
		private const string MESSAGE = "api_message";

		private int apiCode;
		private string apiMessage;

		public PonominaluApiException() { }
		public PonominaluApiException(string message) : base(message) { }
        public PonominaluApiException(string message, Exception inner) : base(message, inner) { }
		private PonominaluApiException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if (info != null)
			{
				ApiCode = info.GetInt32(CODE);
				ApiMessage = info.GetString(MESSAGE);
			}
		}

		public int ApiCode
		{
			get { return apiCode; }
			set { apiCode = value; }
		}
		public string ApiMessage
		{
			get { return apiMessage; }
			set { apiMessage = value; }
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue(CODE, ApiCode);
			info.AddValue(MESSAGE, ApiMessage);
		}
	}
}