﻿using System;

using Example.Ponominalu.Lib.Core.Carts;

namespace Example.Ponominalu.Lib
{
	public sealed class CartFactory
	{
		/// <summary>
		/// Создание пользовательской корзины
		/// </summary>
		/// <param name="userSession">Пользовательская сессия</param>
		/// <returns>Корзина</returns>
		public static ICart Create(string userSession)
		{
			return Activator.CreateInstance(typeof(UserCart), userSession) as ICart;
		}
	}
}