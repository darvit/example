﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Example.Ponominalu.Lib.Data;
using Example.Ponominalu.Lib.Core.Protocol.Data;

namespace Example.Ponominalu.Lib
{
	/// <summary>
	/// Интерфейс сервиса секторов
	/// Предоставляет методы для получения информации о секторах и их схемах
	/// </summary>
	public interface ISectorService
	{
		/// <summary>
		/// Получение доступных секторов события
		/// </summary>
		/// <returns>Список секторов со свободными местами</returns>
		Task<List<Sector>> GetSectors();
		/// <summary>
		/// Получение схемы мест в секторе
		/// </summary>
		/// <param name="sector">ID сектора</param>
		/// <returns>Список рядов в секторе</returns>
		Task<SectorScheme> GetSectorSheme(int sector);
	}
}