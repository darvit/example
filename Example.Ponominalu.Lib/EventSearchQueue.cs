﻿using System;

namespace Example.Ponominalu.Lib
{
	/// <summary>
	/// Запрос поиска событий
	/// </summary>
	public sealed class EventSearchQueue
	{
		/// <summary>
		/// Часть названия
		/// </summary>
		/// <remarks>
		/// Не зависит от регистра
		/// </remarks>
		public string Queue { get; set; }
		/// <summary>
		/// Диапазон поиска от
		/// </summary>
		public DateTime? SearchFrom { get; set; }
		/// <summary>
		/// Диапазон поиска до
		/// </summary>
		public DateTime? SearchTo { get; set; }
	}
}