﻿namespace Example.Ponominalu.Lib.Core
{
	/// <summary>
	/// Интерфейс запроса
	/// </summary>
	internal interface IRequest
	{
		/// <summary>
		/// Сериализация запроса
		/// </summary>
		/// <returns></returns>
		string Serialize();
	}
}