﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Example.Ponominalu.Lib.Core.Protocol.Requests;
using Example.Ponominalu.Lib.Core.Protocol.Responses;
using Example.Ponominalu.Lib.Data;
using CartTicket = Example.Ponominalu.Lib.Core.Protocol.Data.CartTicket;

namespace Example.Ponominalu.Lib.Core.Carts
{
	/// <summary>
	/// Реализация пользовательской корзины
	/// </summary>
	internal sealed class UserCart : ICart
	{
		#region Fields
		/// <summary>
		/// Провайдер данных
		/// </summary>
		private IProvider m_Provider;
		/// <summary>
		/// Пользовательская сессия
		/// </summary>
		private string m_UserSession;
		#endregion

		#region Constructors
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <param name="userSession">Пользовательская сессия</param>
		public UserCart(string userSession)
		{
			if (String.IsNullOrEmpty(userSession))
			{
				throw new ArgumentNullException("userSession");
			}

			m_UserSession = userSession;
			m_Provider = ProviderFactory.Create();
			Tickets = new List<Ticket>();
		}
		#endregion

		#region Properties
		/// <summary>
		/// Общая стоимость билетов в корзине
		/// </summary>
		public int Amount { get; private set; }
		/// <summary>
		/// Комиссия
		/// </summary>
		public int Commission { get; private set; }
		/// <summary>
		/// Список билетов
		/// </summary>
		public List<Ticket> Tickets { get; private set; }
		#endregion

		#region Public Methods
		/// <summary>
		/// Добавить билет
		/// </summary>
		/// <param name="id">ID билета</param>
		/// <param name="eventId">ID события</param>
		/// <param name="sectorId">ID сектора</param>
		/// <param name="price">Цена билета</param>
		/// <returns>Статус билета: true - билет забронирован и добавлен в корзину</returns>
		public async Task<bool> AddTicket(int id, int eventId, int sectorId, int price)
		{
			IRequest request = Activator.CreateInstance(typeof(AddTicketRequest), id, m_UserSession, eventId, sectorId) as IRequest;
			AddTicketResponse response = await m_Provider.SendRequest<AddTicketResponse>(request);
			if (response.Success)
			{
				Ticket ticket = new Ticket()
				{
					EventId = eventId,
					Id = id,
					SectorId = sectorId,
					Price = price
				};
				Tickets.Add(ticket);
			}
			return response.Success;
		}
		/// <summary>
		/// Доавить входные билеты
		/// </summary>
		/// <param name="eventId">ID события</param>
		/// <param name="sectorId">ID сектора</param>
		/// <param name="price">Цена билета</param>
		/// <param name="count">Колчество билетов</param>
		/// <returns>Статус билетов: true - билеты забронированы и добавлены в корзину</returns>
		public async Task<bool> AddAdmission(int eventId, int sectorId, int price, int count)
		{
			IRequest admissionRequest = Activator.CreateInstance(typeof(AddAdmissionRequest), eventId, sectorId, price, count, m_UserSession) as IRequest;
			AddAdmisssionResponse admissionResponse = await m_Provider.SendRequest<AddAdmisssionResponse>(admissionRequest);
			if (admissionResponse.Success)
			{
				IRequest cartRequest = Activator.CreateInstance(typeof(CartRequest), m_UserSession) as IRequest;
				CartResponse cartResponse = await m_Provider.SendRequest<CartResponse>(cartRequest);
				foreach (CartTicket ticket in cartResponse.Cart.Tickets)
				{
					if (Tickets.Where(t => t.Id == ticket.Id).Count() == 0)
					{
						Tickets.Add(new Ticket()
						{
							Id = ticket.Id,
							EventId = eventId,
							Price = price,
							SectorId = sectorId
						});
					}
				}
			}

			return admissionResponse.Success;
		}
		/// <summary>
		/// Убрать билет из корзины
		/// </summary>
		/// <param name="ticket">ID билета</param>
		/// <returns>Статус билета: true - билет удален из корзиы</returns>
		public async Task<bool> RemoveTicket(int ticket)
		{
			Ticket removeTicket = Tickets.FirstOrDefault(t => t.Id == ticket);
			if (removeTicket == null)
			{
				return true;
			}
			IRequest request = Activator.CreateInstance(typeof(RemoveTicketRequest), ticket, m_UserSession) as IRequest;
			RemoveTicketResponse response = await m_Provider.SendRequest<RemoveTicketResponse>(request);
			if (response.Success)
			{
				Tickets.Remove(removeTicket);
			}
			return response.Success;
		}
		/// <summary>
		/// Очистка корзины
		/// </summary>
		/// <returns>true - все билеты удалены</returns>
		public async Task<bool> Clear()
		{
			IRequest request = Activator.CreateInstance(typeof(ClearCartRequest), m_UserSession) as IRequest;
			ClearCartResponse response = await m_Provider.SendRequest<ClearCartResponse>(request);
			return response.Success;
		}

		/// <summary>
		/// Создать заказ на основе корзины
		/// </summary>
		/// <returns></returns>
		public IOrder CreateOrder()
		{
			return OrderFactory.Create(m_UserSession);
		}
		#endregion
	}
}