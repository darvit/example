﻿using System;

using Example.Ponominalu.Lib.Core.Orders;

namespace Example.Ponominalu.Lib.Core
{
	internal sealed class OrderFactory
	{
		public static IOrder Create(string userSession)
		{
			return Activator.CreateInstance(typeof(UserOrder), userSession) as IOrder;
		}
	}
}