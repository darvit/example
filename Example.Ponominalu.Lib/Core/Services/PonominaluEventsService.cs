﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Example.Ponominalu.Lib.Core.Protocol;
using Example.Ponominalu.Lib.Core.Protocol.Requests;
using Example.Ponominalu.Lib.Core.Protocol.Responses;
using Example.Ponominalu.Lib.Data;

namespace Example.Ponominalu.Lib.Core.Services
{
	/// <summary>
	/// Реализация сервиса поиска событий
	/// </summary>
	internal sealed class PonominaluEventsService : IEventsService
	{
		#region Fields
		/// <summary>
		/// Провайдер данных
		/// </summary>
		private IProvider m_Provider;
		#endregion

		#region Constructors
		/// <summary>
		/// Инициализация
		/// </summary>
		public PonominaluEventsService()
		{
			m_Provider = ProviderFactory.Create();
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Получение мероприятий из категории
		/// </summary>
		/// <param name="category">Категория</param>
		/// <returns>Список событий</returns>
		public async Task<List<EventPreview>> GetEventsFromCategory(ECategory category)
		{
			return await GetEvents((int)category, null, null, null);
		}
		/// <summary>
		/// Поиск событий
		/// </summary>
		/// <param name="queue">Запрос поиска</param>
		/// <returns>Мероприятия, удовлетворяющие критериям поиска</returns>
		public async Task<List<EventPreview>> SearchEvents(EventSearchQueue queue)
		{
			return await GetEvents(0, queue.Queue, queue.SearchFrom, queue.SearchTo);
		}
		/// <summary>
		/// Получение подробной информации о мероприятии по ID
		/// </summary>
		/// <param name="id">ID мероприятия</param>
		/// <returns>Расширенная информация</returns>
		public async Task<Event> GetEvent(int id)
		{
			IRequest request = Activator.CreateInstance(typeof(SubEventsRequest), id) as IRequest;
			SubEventsResponse rsp = await m_Provider.SendRequest<SubEventsResponse>(request);
			IRequest descriptionRequest = Activator.CreateInstance(typeof(EventDescriptionRequest), id) as IRequest;
			EventDescriptionResponse descriptionResponse = await m_Provider.SendRequest<EventDescriptionResponse>(descriptionRequest);
			rsp.Event.Description = descriptionResponse.Description;
			return rsp.Event;
		}
		#endregion

		#region Private Methods
		/// <summary>
		/// Запрос событий
		/// </summary>
		/// <param name="category">Категория событий</param>
		/// <param name="queue">Строка для поиска по названию</param>
		/// <param name="from">Дата от</param>
		/// <param name="to">Дата до</param>
		/// <returns>Список найденных событий</returns>
		private async Task<List<EventPreview>> GetEvents(int category, string queue, DateTime? from, DateTime? to)
		{
			IRequest request = Activator.CreateInstance(typeof(EventsRequest), category, queue, from, to) as IRequest;
			EventsResponse response = await m_Provider.SendRequest<EventsResponse>(request);
			return response.Events;
		}
		#endregion
	}
}