﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Example.Ponominalu.Lib.Core.Protocol.Data;
using Example.Ponominalu.Lib.Core.Protocol.Requests;
using Example.Ponominalu.Lib.Core.Protocol.Responses;
using Example.Ponominalu.Lib.Data;
using PTicket = Example.Ponominalu.Lib.Core.Protocol.Data.Ticket;

namespace Example.Ponominalu.Lib.Core.Services
{
	/// <summary>
	/// Реализация сервиса секторов
	/// </summary>
	internal sealed class PonominaluSectorService : ISectorService
	{
		#region Fields
		/// <summary>
		/// ID события
		/// </summary>
		private int m_EventId;
		/// <summary>
		/// Провайдер данных
		/// </summary>
		private IProvider m_Provider;
		#endregion

		#region Constructors
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <param name="eventId">ID события</param>
		public PonominaluSectorService(int eventId)
		{
			if (eventId <= 0)
			{
				throw new ArgumentOutOfRangeException("eventId");
			}
			m_EventId = eventId;
			m_Provider = ProviderFactory.Create();
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Получение доступных секторов события
		/// </summary>
		/// <returns>Список секторов со свободными местами</returns>
		public async Task<List<Sector>> GetSectors()
		{
			IRequest request = Activator.CreateInstance(typeof(SectorsRequest), m_EventId) as IRequest;
			SectorsResponse response = await m_Provider.SendRequest<SectorsResponse>(request);
			return response.Sectors;
		}
		/// <summary>
		/// Получение схемы мест в секторе
		/// </summary>
		/// <param name="sector">ID сектора</param>
		/// <returns>Список рядов в секторе</returns>
		public async Task<SectorScheme> GetSectorSheme(int sector)
		{
			IRequest sectorRequest = Activator.CreateInstance(typeof(SectorRequest), sector) as IRequest;
			SectorResponse sectorResponse = await m_Provider.SendRequest<SectorResponse>(sectorRequest);
			IRequest ticketsRequest = Activator.CreateInstance(typeof(SectorTicketsRequest), m_EventId, sector) as IRequest;
			SectorTicketsResponse ticketsResponse = await m_Provider.SendRequest<SectorTicketsResponse>(ticketsRequest);

			SectorScheme scheme = new SectorScheme()
			{
				Id = sectorResponse.Scheme.Id,
				Title = sectorResponse.Scheme.Title,
				Admission = sectorResponse.Scheme.Admission,
				Rows = new List<Row>()
			};
			foreach (SectorRow row in sectorResponse.Scheme.Rows)
			{
				Row r = new Row() { Number = row.Number, Seats = new List<Seat>() };
				foreach (SectorSeat seat in row.Seats)
				{
					Seat s = new Seat() { Number = seat.Number };
					PTicket ticket = ticketsResponse.Tickets.FirstOrDefault(sa => sa.Row == row.Number && sa.Seat == seat.Number);
					if (ticket != null)
					{
						s.Id = ticket.Id;
						s.Price = ticket.Price;
						s.Reserved = false;
					}
					r.Seats.Add(s);
				}
				scheme.Rows.Add(r);
			}

			return scheme;
		}
		#endregion
	}
}