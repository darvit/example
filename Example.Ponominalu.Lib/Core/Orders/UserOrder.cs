﻿using System;
using System.Threading.Tasks;

using Example.Ponominalu.Lib.Core.Protocol.Requests;
using Example.Ponominalu.Lib.Core.Protocol.Responses;

namespace Example.Ponominalu.Lib.Core.Orders
{
	/// <summary>
	/// Реализация заказа
	/// </summary>
	internal sealed class UserOrder : IOrder
	{
		#region Fields
		/// <summary>
		/// Пользовательская сессия
		/// </summary>
		private string m_UserSession;
		/// <summary>
		/// Провайдер данных
		/// </summary>
		private IProvider m_Provider;
		#endregion

		#region Constructors
		/// <summary>
		/// Инициализация
		/// </summary>
		/// <param name="userSession">Пользовательская сессия</param>
		public UserOrder(string userSession)
		{
			if (String.IsNullOrEmpty(userSession))
			{
				throw new ArgumentNullException("userSession");
			}

			m_UserSession = userSession;
			m_Provider = ProviderFactory.Create();
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Обновление информации о покупателе
		/// </summary>
		/// <param name="name">Имя клиента</param>
		/// <param name="phone">Телефон для связи</param>
		/// <param name="email">Email для отправки билетов</param>
		/// <returns>true - успешное обновление</returns>
		public async Task<bool> UpdateOrder(string name, string phone, string email)
		{
			IRequest request = Activator.CreateInstance(typeof(CreateOrderRequest), name, phone, email, m_UserSession) as IRequest;
			CreateOrderResponse response = await m_Provider.SendRequest<CreateOrderResponse>(request);
			return response.Success;
		}
		/// <summary>
		/// Закрытие заказа
		/// </summary>
		/// <returns>true - заказ выполнен</returns>
		public async Task<bool> FinishOrder()
		{
			IRequest request = Activator.CreateInstance(typeof(CreateOrderRequest), m_UserSession) as IRequest;
			CreateOrderResponse response = await m_Provider.SendRequest<CreateOrderResponse>(request);
			return response.Success;
		}
		#endregion
	}
}