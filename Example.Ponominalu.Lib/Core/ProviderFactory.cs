﻿using System;
using System.Configuration;

using Example.Ponominalu.Lib.Core.Providers;

namespace Example.Ponominalu.Lib.Core
{
	/// <summary>
	/// Фабрика провайдеров данных
	/// </summary>
	internal sealed class ProviderFactory
	{
		/// <summary>
		/// Создание экземпляра провайдера
		/// </summary>
		/// <returns>Провайдер данных</returns>
		public static IProvider Create()
		{
			string api = ConfigurationManager.AppSettings.Get("ponominalu_api");
			string session = ConfigurationManager.AppSettings.Get("ponominalu_session");

			if (String.IsNullOrEmpty(api))
			{
				throw new ConfigurationException("API url not found in config");
			}
			if (String.IsNullOrEmpty(session))
			{
				throw new ConfigurationException("App session not found in config");
			}

			return Activator.CreateInstance(typeof(Provider), api, session) as IProvider;
		}
	}
}