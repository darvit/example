﻿namespace Example.Ponominalu.Lib.Core
{
	/// <summary>
	/// Интерфейс ответа
	/// </summary>
	internal interface IResponse
	{
		/// <summary>
		/// Разбор ответа
		/// </summary>
		/// <param name="response">Данные ответа</param>
		void Parse(dynamic response);
	}
}