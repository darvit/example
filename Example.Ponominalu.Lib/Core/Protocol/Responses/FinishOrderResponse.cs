﻿using Newtonsoft.Json.Linq;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class FinishOrderResponse : IResponse
	{
		public bool Success { get; set; }

		public void Parse(dynamic response)
		{
			JValue value = response as JValue;
			if (value != null)
			{
				int result = value.ToObject<int>();
				Success = result > 0;
			}
		}
	}
}