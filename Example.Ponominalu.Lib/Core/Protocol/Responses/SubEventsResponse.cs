﻿using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using Example.Ponominalu.Lib.Data;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class SubEventsResponse : IResponse
	{
		public Event Event { get; set; }

		public void Parse(dynamic response)
		{
			JObject o = response as JObject;
			if (o != null)
			{
				Event = o.ToObject<Event>();
				JArray array = response.@event.subevents as JArray;
				if (array != null)
				{
					Event.EventDates = array.ToObject<List<EventDate>>();
				}
			}
		}
	}
}