﻿using System;
using System.Text;

using Newtonsoft.Json.Linq;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	/// <summary>
	/// Ответ на запрос описания события
	/// </summary>
	internal sealed class EventDescriptionResponse : IResponse
	{
		public string Description { get; set; }

		public void Parse(dynamic response)
		{
			JValue description = response as JValue;
			if (description != null)
			{
				Description = Encoding.UTF8.GetString(Convert.FromBase64String(description.ToString()));
			}
		}
	}
}