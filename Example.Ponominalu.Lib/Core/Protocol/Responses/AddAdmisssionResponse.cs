﻿using Newtonsoft.Json.Linq;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class AddAdmisssionResponse : IResponse
	{
		public bool Success { get; set; }

		public void Parse(dynamic response)
		{
			JValue value = response as JValue;
			if (value != null)
			{
				int count = value.ToObject<int>();
				Success = count > 0;
			}
		}
	}
}