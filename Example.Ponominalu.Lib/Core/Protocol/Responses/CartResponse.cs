﻿using Example.Ponominalu.Lib.Core.Protocol.Data;
using Newtonsoft.Json.Linq;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class CartResponse : IResponse
	{
		public Cart Cart { get; set; }

		public void Parse(dynamic response)
		{
			JObject cart = response as JObject;
			if (cart != null)
			{
				Cart = cart.ToObject<Cart>();
			}
		}
	}
}