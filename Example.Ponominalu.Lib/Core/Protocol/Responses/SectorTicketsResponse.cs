﻿using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using Example.Ponominalu.Lib.Core.Protocol.Data;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class SectorTicketsResponse : IResponse
	{
		public List<Ticket> Tickets { get; set; }

		public void Parse(dynamic response)
		{
			JArray tickets = response as JArray;
			if (tickets != null)
			{
				Tickets = tickets.ToObject<List<Ticket>>();
			}
		}
	}
}