﻿using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using Example.Ponominalu.Lib.Data;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class EventsResponse : IResponse
	{
		public List<EventPreview> Events;

		public void Parse(dynamic response)
		{
			JArray a = response as JArray;
			Events = a.ToObject<List<EventPreview>>();
		}
	}
}