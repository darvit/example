﻿using Newtonsoft.Json.Linq;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class ClearCartResponse : IResponse
	{
		private const string SUCCESS_RESPONSE = "ok";
			
		public bool Success { get; set; }

		public void Parse(dynamic response)
		{
			JValue result = response as JValue;
			if (result != null)
			{
				Success = result.ToObject<string>() == SUCCESS_RESPONSE;
			}
		}
	}
}