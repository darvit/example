﻿using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using Example.Ponominalu.Lib.Data;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class SectorsResponse : IResponse
	{
		public List<Sector> Sectors { get; set; }

		public void Parse(dynamic response)
		{
			JArray array = response.sectors as JArray;
			if (array != null)
			{
				Sectors = array.ToObject<List<Sector>>();
			}
		}
	}
}