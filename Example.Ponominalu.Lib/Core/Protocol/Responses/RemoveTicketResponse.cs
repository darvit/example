﻿using Newtonsoft.Json.Linq;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class RemoveTicketResponse : IResponse
	{
		public bool Success { get; set; }

		public void Parse(dynamic response)
		{
			JValue result = response as JValue;
			if (result != null)
			{
				Success = result.ToObject<int>() == 1;
			}
		}
	}
}