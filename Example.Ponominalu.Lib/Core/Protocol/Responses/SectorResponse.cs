﻿using Newtonsoft.Json.Linq;

using Example.Ponominalu.Lib.Core.Protocol.Data;

namespace Example.Ponominalu.Lib.Core.Protocol.Responses
{
	internal sealed class SectorResponse : IResponse
	{
		public SectorResponseContent Scheme { get; set; }

		public void Parse(dynamic response)
		{
			JObject scheme = response as JObject;
			if (scheme != null)
			{
				Scheme = scheme.ToObject<SectorResponseContent>();
			}
		}
	}
}