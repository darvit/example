﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class ClearCartRequest : IRequest
	{
		private const string METHOD = "clear_cart?";

		private string m_UserSession;

		public ClearCartRequest(string userSession)
		{
			if (String.IsNullOrEmpty(userSession))
			{
				throw new ArgumentNullException("userSession");
			}
			m_UserSession = userSession;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);
			request.Append(String.Format("user_session={0}&", m_UserSession));
			return request.ToString();
		}
	}
}