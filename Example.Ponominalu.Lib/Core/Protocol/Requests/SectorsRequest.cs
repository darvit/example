﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class SectorsRequest : IRequest
	{
		private const string METHOD = "get_subevent?";

		private int m_EventId;

		public SectorsRequest(int eventId)
		{
			if (eventId <= 0)
			{
				throw new ArgumentOutOfRangeException("eventId", "Event ID must be above zero");
			}
			m_EventId = eventId;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);
			request.Append(String.Format("id={0}&", m_EventId));
			return request.ToString();
		}
	}
}