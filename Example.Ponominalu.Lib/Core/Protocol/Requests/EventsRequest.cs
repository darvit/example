﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol
{
	/// <summary>
	/// Запрос списка событий
	/// </summary>
	internal sealed class EventsRequest : IRequest
	{
		#region Constants
		/// <summary>
		/// Метод API Пономиналу для запроса списка событий
		/// </summary>
		private const string METHOD = "get_events?";
		#endregion

		#region Fields
		/// <summary>
		/// Категория событий
		/// </summary>
		private int m_Category;
		/// <summary>
		///	Фильтр по заголовку события
		/// </summary>
		private string m_Queue;
		/// <summary>
		/// Фильтр по дате от
		/// </summary>
		private DateTime? m_Start;
		/// <summary>
		/// Фильтр по дате до
		/// </summary>
		private DateTime? m_End;
		#endregion

		#region Constructors
		public EventsRequest(int category, string queue, DateTime? start, DateTime? end)
		{
			m_Category = category;
			m_Queue = queue;
			m_Start = start;
			m_End = end;
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Сериализация в строку для GET запроса
		/// </summary>
		/// <returns>Тело запроса</returns>
		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);

			if (m_Category > 0)
			{
				request.Append(String.Format("category={0}&", m_Category));
			}
			if (!String.IsNullOrEmpty(m_Queue))
			{
				request.Append(String.Format("title={0}&", m_Queue));
			}
			if (m_End.HasValue)
			{
				request.Append(String.Format("max_date={0}&", m_End.Value.ToString("yyyy-MM-dd")));
			}
			// Если не указан фильтр по дате от по умолчанию ищем события от сегодня
			if (!m_Start.HasValue)
			{
				m_Start = DateTime.Now;
			}
			request.Append(String.Format("min_date={0}&", m_Start.Value.ToString("yyyy-MM-dd")));

			return request.ToString();
		}
		#endregion
	}
}