﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class SectorRequest : IRequest
	{
		private const string METHOD = "get_sector?";

		private int m_SectorId;
		
		public SectorRequest(int sectorId)
		{
			if (sectorId <= 0)
			{
				throw new ArgumentOutOfRangeException("sectorId");
			}
			m_SectorId = sectorId;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);
			request.Append(String.Format("sector_id={0}&", m_SectorId));
			return request.ToString();
		}
	}
}