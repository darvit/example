﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class RemoveTicketRequest : IRequest
	{
		private const string METHOD = "drop_ticket?";

		private int m_TicketId;
		private string m_UserSession;

		public RemoveTicketRequest(int ticketId, string userSession)
		{
			if (ticketId <= 0)
			{
				throw new ArgumentOutOfRangeException("ticketId");
			}
			if (String.IsNullOrEmpty(userSession))
			{
				throw new ArgumentNullException("userSession");
			}
			m_TicketId = ticketId;
			m_UserSession = userSession;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);

			request.Append(String.Format("ticket_id={0}&", m_TicketId));
			request.Append(String.Format("user_session={0}&", m_UserSession));

			return request.ToString();
		}
	}
}