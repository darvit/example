﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class CartRequest : IRequest
	{
		private const string METHOD = "get_cart?";

		private string m_UserSession;

		public CartRequest(string userSession)
		{
			m_UserSession = userSession;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);
			request.Append(String.Format("user_session={0}&", m_UserSession));
			return request.ToString();
		}
	}
}