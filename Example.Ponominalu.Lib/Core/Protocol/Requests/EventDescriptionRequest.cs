﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class EventDescriptionRequest : IRequest
	{
		#region Constants
		/// <summary>
		/// Метод API Пономиналу для запроса описания события
		/// </summary>
		private const string METHOD = "get_description?";
		#endregion

		private int m_EventId;

		public EventDescriptionRequest(int eventId)
		{
			if (eventId <= 0)
			{
				throw new ArgumentOutOfRangeException("eventId", "Event ID must be above zero");
			}
			m_EventId = eventId;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);
			request.Append(String.Format("subevent_id={0}&", m_EventId));
			return request.ToString();
		}
	}
}