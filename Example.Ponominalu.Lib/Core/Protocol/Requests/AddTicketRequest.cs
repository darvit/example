﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class AddTicketRequest : IRequest
	{
		#region Constants
		/// <summary>
		/// Метод API Пономиналу для запроса описания события
		/// </summary>
		private const string METHOD = "add_ticket?";
		#endregion

		private int m_TicketId;
		private string m_UserSession;
		private int m_EventId;
		private int m_SectorId;

		public AddTicketRequest(int ticketId, string userSession, int eventId, int sectorId)
		{
			if (ticketId <= 0)
			{
				throw new ArgumentOutOfRangeException("ticketId");
			}
			if (String.IsNullOrEmpty(userSession))
			{
				throw new ArgumentNullException("userSession");
			}
			if (eventId <= 0)
			{
				throw new ArgumentOutOfRangeException("eventId");
			}
			if (sectorId <= 0)
			{
				throw new ArgumentOutOfRangeException("sectorId");
			}
			if (String.IsNullOrEmpty(userSession))
			{
				throw new ArgumentNullException("userSession");
			}

			m_TicketId = ticketId;
			m_UserSession = userSession;
			m_EventId = eventId;
			m_SectorId = sectorId;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);
			request.Append(String.Format("ticket_id={0}&", m_TicketId));
			request.Append(String.Format("user_session={0}&", m_UserSession));
			request.Append(String.Format("subevent_id={0}&", m_EventId));
			request.Append(String.Format("sector_id={0}&", m_SectorId));
			return request.ToString();
		}
	}
}