﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class AddAdmissionRequest : IRequest
	{
		private const string METHOD = "add_admission?";

		private int m_EventId;
		private int m_SectorId;
		private int m_Price;
		private int m_Count;
		private string m_UserSession;

		public AddAdmissionRequest(int eventId, int sectorId, int price, int count, string userSession)
		{
			if (eventId <= 0)
			{
				throw new ArgumentOutOfRangeException("eventId");
			}
			if (sectorId <= 0)
			{
				throw new ArgumentOutOfRangeException("sectorId");
			}
			if (price <= 0)
			{
				throw new ArgumentOutOfRangeException("price");
			}
			if (count <= 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (String.IsNullOrEmpty(userSession))
			{
				throw new ArgumentNullException("userSession");
			}

			m_EventId = eventId;
			m_SectorId = sectorId;
			m_Price = price;
			m_Count = count;
			m_UserSession = userSession;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);

			request.Append(String.Format("subevent_id={0}&", m_EventId));
			request.Append(String.Format("sector_id={0}&", m_SectorId));
			request.Append(String.Format("price={0}&", m_Price));
			request.Append(String.Format("count={0}&", m_Count));
			request.Append(String.Format("user_session={0}&", m_UserSession));

			return request.ToString();
		}
	}
}