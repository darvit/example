﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class SectorTicketsRequest : IRequest
	{
		private const string METHOD = "get_tickets?";

		private int m_EventId;
		private int m_SectorId;

		public SectorTicketsRequest(int eventId, int sectorId)
		{
			if (eventId <= 0)
			{
				throw new ArgumentOutOfRangeException("eventId");
			}
			if (sectorId <= 0)
			{
				throw new ArgumentOutOfRangeException("sectorId");
			}

			m_EventId = eventId;
			m_SectorId = sectorId;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);

			request.Append(String.Format("subevent_id={0}&", m_EventId));
			request.Append(String.Format("sector_id={0}&", m_SectorId));

			return request.ToString();
		}
	}
}