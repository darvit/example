﻿using System;
using System.Text;

namespace Example.Ponominalu.Lib.Core.Protocol.Requests
{
	internal sealed class CreateOrderRequest : IRequest
	{
		private const string METHOD = "update_order?";

		private string m_Name;
		private string m_Phone;
		private string m_Email;
		private string m_UserSession;

		public CreateOrderRequest(string name, string phone, string email, string userSession)
		{
			if (String.IsNullOrEmpty(name))
			{
				throw new ArgumentNullException("name");
			}
			if (String.IsNullOrEmpty(phone))
			{
				throw new ArgumentNullException("phone");
			}
			if (String.IsNullOrEmpty(email))
			{
				throw new ArgumentNullException("email");
			}
			if (String.IsNullOrEmpty(userSession))
			{
				throw new ArgumentNullException("userSession");
			}
			m_Name = name;
			m_Phone = phone;
			m_Email = email;
			m_UserSession = userSession;
		}

		public string Serialize()
		{
			StringBuilder request = new StringBuilder(METHOD);

			request.Append(String.Format("name={0}&", m_Name));
			request.Append(String.Format("phone={0}&", m_Phone));
			request.Append(String.Format("email={0}&", m_Email));
			request.Append(String.Format("user_session={0}&", m_UserSession));

			return request.ToString();
		}
	}
}