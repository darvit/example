﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Core.Protocol.Data
{
	internal sealed class SectorResponseContent
	{
		[JsonProperty(PropertyName="id")]
		public int Id { get; set; }
		[JsonProperty(PropertyName = "title")]
		public string Title { get; set; }
		[JsonProperty(PropertyName = "admission")]
		public bool Admission { get; set; }
		[JsonProperty(PropertyName = "content")]
		public List<SectorRow> Rows { get; set; }
	}

	internal sealed class SectorRow
	{
		[JsonProperty(PropertyName = "number")]
		public int Number { get; set; }
		[JsonProperty(PropertyName = "seats")]
		public List<SectorSeat> Seats { get; set; }
	}

	internal sealed class SectorSeat
	{
		[JsonProperty(PropertyName = "n")]
		public int Number { get; set; }
	}
}