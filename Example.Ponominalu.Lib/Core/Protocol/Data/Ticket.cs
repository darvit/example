﻿using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Core.Protocol.Data
{
	public sealed class Ticket
	{
		[JsonProperty(PropertyName="id")]
		public int Id { get; set; }
		[JsonProperty(PropertyName = "price")]
		public int Price { get; set; }
		[JsonProperty(PropertyName = "row")]
		public int Row { get; set; }
		[JsonProperty(PropertyName = "seat")]
		public int Seat { get; set; }
	}
}