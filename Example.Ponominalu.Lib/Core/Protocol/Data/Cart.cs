﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Core.Protocol.Data
{
	internal sealed class Cart
	{
		[JsonProperty(PropertyName="count")]
		public int Count { get; set; }
		[JsonProperty(PropertyName = "sum")]
		public int Amount { get; set; }
		[JsonProperty(PropertyName = "commission_amount")]
		public int Commission { get; set; }
		[JsonProperty(PropertyName = "tickets")]
		public List<CartTicket> Tickets { get; set; }
	}

	internal sealed class CartTicket
	{
		[JsonProperty(PropertyName = "id")]
		public int Id { get; set; }
	}
}