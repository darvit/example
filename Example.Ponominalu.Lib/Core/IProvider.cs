﻿using System.Threading.Tasks;

namespace Example.Ponominalu.Lib.Core
{
	/// <summary>
	/// Интерфейс провайдера данных
	/// </summary>
	internal interface IProvider
	{
		/// <summary>
		/// Отправка запроса
		/// </summary>
		/// <typeparam name="T">Тип ответа</typeparam>
		/// <param name="request">Запрос</param>
		/// <returns>Ответ источника данных</returns>
		/// <exception cref="PonominaluApiException">
		/// Выбрасывает исключение в случае ошибки при обращении к данным
		/// </exception>
		Task<T> SendRequest<T>(IRequest request) where T : IResponse;
	}
}