﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Core.Providers
{
	/// <summary>
	/// Провайдер данных
	/// </summary>
	internal sealed class Provider : IProvider
	{
		/// <summary>
		/// Адрес для запросов
		/// </summary>
		private readonly string API;
		/// <summary>
		/// Сессия приложения
		/// </summary>
		private readonly string APP_SESSION;

		/// <summary>
		/// Инициализация провайдера
		/// </summary>
		/// <param name="api">Адрес для запросов</param>
		/// <param name="session">Сессия приложения</param>
		public Provider(string api, string session)
		{
			if (String.IsNullOrEmpty(api))
			{
				throw new ArgumentNullException("api");
			}
			if (String.IsNullOrEmpty(session))
			{
				throw new ArgumentNullException("session");
			}

			API = api;
			APP_SESSION = session;
		}

		/// <summary>
		/// Отправка запроса
		/// </summary>
		/// <typeparam name="T">Тип ответа</typeparam>
		/// <param name="request">Запрос</param>
		/// <returns>Ответ источника данных</returns>
		public async Task<T> SendRequest<T>(IRequest request) where T : IResponse
		{
			StringBuilder url = new StringBuilder(API);
			url.Append(request.Serialize());
			url.Append(String.Format("session={0}", APP_SESSION));
			
			HttpWebRequest http = WebRequest.Create(url.ToString()) as HttpWebRequest;
			HttpWebResponse response = await http.GetResponseAsync() as HttpWebResponse;
			using (StreamReader reader = new StreamReader(response.GetResponseStream()))
			{
				string content = await reader.ReadToEndAsync();
				dynamic message = JsonConvert.DeserializeObject(content);
				if (message.code == 1)
				{
					T responseContent = Activator.CreateInstance<T>();
					responseContent.Parse(message.message);
					return responseContent;
				}

				PonominaluApiException exc = new PonominaluApiException("API returned error");
				exc.ApiCode = message.code;
				exc.ApiMessage = message.message;
				throw exc;
			}
		}
	}
}