﻿using System.Threading.Tasks;

namespace Example.Ponominalu.Lib
{
	/// <summary>
	/// Интерфейс заказа билетов
	/// </summary>
	public interface IOrder
	{
		/// <summary>
		/// Обновление информации о покупателе
		/// </summary>
		/// <param name="name">Имя клиента</param>
		/// <param name="phone">Телефон для связи</param>
		/// <param name="email">Email для отправки билетов</param>
		/// <returns>true - успешное обновление</returns>
		Task<bool> UpdateOrder(string name, string phone, string email);
		/// <summary>
		/// Закрытие заказа
		/// </summary>
		/// <returns>true - заказ выполнен</returns>
		Task<bool> FinishOrder();
	}
}