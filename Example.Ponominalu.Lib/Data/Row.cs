﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Data
{
	/// <summary>
	/// Ряд сектора
	/// </summary>
	public sealed class Row
	{
		/// <summary>
		/// Номер ряда
		/// </summary>
		[JsonProperty("number")]
		public int Number { get; set; }
		/// <summary>
		/// Места в ряду
		/// </summary>
		[JsonProperty("seats")]
		public List<Seat> Seats { get; set; }
	}
}