﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Data
{
	/// <summary>
	/// Событие
	/// </summary>
	public sealed class Event
	{
		/// <summary>
		/// ID
		/// </summary>
		[JsonProperty(PropertyName = "id")]
		public int Id { get; set; }
		/// <summary>
		/// Название
		/// </summary>
		[JsonProperty(PropertyName = "title")]
		public string Title { get; set; }
		/// <summary>
		/// Дата начала события
		/// </summary>
		[JsonProperty(PropertyName = "str_date")]
		public string StartDate { get; set; }
		/// <summary>
		/// Время начала события
		/// </summary>
		[JsonProperty(PropertyName = "str_time")]
		public string StartTime { get; set; }
		/// <summary>
		/// Постер к событию
		/// </summary>
		[JsonProperty(PropertyName = "original_image")]
		public string Image { get; set; }
		/// <summary>
		/// Минимальная цена билетов
		/// </summary>
		[JsonProperty(PropertyName = "min_price")]
		public int MinPrice { get; set; }
		/// <summary>
		/// Максимальная цена билетов
		/// </summary>
		[JsonProperty(PropertyName = "max_price")]
		public int MaxPrice { get; set; }
		/// <summary>
		/// Количество доступных билетов
		/// </summary>
		[JsonProperty(PropertyName = "ticket_count")]
		public int TicketCount { get; set; }
		/// <summary>
		/// Описание события
		/// </summary>
		[JsonProperty(PropertyName="description")]
		public string Description { get; set; }
		/// <summary>
		/// Место проведения события
		/// </summary>
		[JsonProperty(PropertyName = "venue")]
		public Venue Venue { get; set; }
		/// <summary>
		/// Список дат события
		/// </summary>
		[JsonProperty(PropertyName = "subevents")]
		public List<EventDate> EventDates { get; set; }
	}
}