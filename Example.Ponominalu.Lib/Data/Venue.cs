﻿using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Data
{
	/// <summary>
	/// Место проведения события
	/// </summary>
	public sealed class Venue
	{
		/// <summary>
		/// ID площадки
		/// </summary>
		[JsonProperty(PropertyName = "id")]
		public int Id { get; set; }
		/// <summary>
		/// Название площадки
		/// </summary>
		[JsonProperty(PropertyName = "title")]
		public string Title { get; set; }
		/// <summary>
		/// Адрес площадки
		/// </summary>
		[JsonProperty(PropertyName = "address")]
		public string Address { get; set; }
	}
}