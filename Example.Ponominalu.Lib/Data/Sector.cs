﻿using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Data
{
	/// <summary>
	/// Сектор с билетами
	/// </summary>
	public sealed class Sector
	{
		/// <summary>
		/// ID секьора
		/// </summary>
		[JsonProperty(PropertyName="id")]
		public int Id { get; set; }
		/// <summary>
		/// Название сектора
		/// </summary>
		[JsonProperty(PropertyName = "title")]
		public string Title { get; set; }
		/// <summary>
		/// Количество доступных билетов
		/// </summary>
		[JsonProperty(PropertyName = "count")]
		public int Count { get; set; }
		/// <summary>
		/// Минимальная цена билета
		/// </summary>
		[JsonProperty(PropertyName = "min_price")]
		public int MinPrice { get; set; }
		/// <summary>
		/// Максимальная цена билета
		/// </summary>
		[JsonProperty(PropertyName = "max_price")]
		public int MaxPrice { get; set; }
		/// <summary>
		/// Признак входного билета
		/// </summary>
		/// <remarks>
		/// В системе существует два типа билетов - входной и с определенным местом
		/// </remarks>
		[JsonProperty(PropertyName = "admission")]
		public bool Admission { get; set; }
	}
}