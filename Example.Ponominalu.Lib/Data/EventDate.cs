﻿using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Data
{
	/// <summary>
	/// Дата проведения события
	/// </summary>
	public sealed class EventDate
	{
		/// <summary>
		/// ID
		/// </summary>
		[JsonProperty(PropertyName="id")]
		public int Id { get; set; }
		/// <summary>
		/// Дата начала
		/// </summary>
		[JsonProperty(PropertyName = "str_date")]
		public string Date { get; set; }
		/// <summary>
		/// Время начала
		/// </summary>
		[JsonProperty(PropertyName = "str_time")]
		public string Time { get; set; }
	}
}