﻿using System.Collections.Generic;

namespace Example.Ponominalu.Lib.Data
{
	/// <summary>
	/// Схема сектора
	/// </summary>
	public sealed class SectorScheme
	{
		public int Id { get; set; }
		public string Title { get; set; }
		/// <summary>
		/// Признак сектора без мест (например танцпол)
		/// </summary>
		public bool Admission { get; set; }
		/// <summary>
		/// Схема мест
		/// </summary>
		public List<Row> Rows { get; set; }
	}
}