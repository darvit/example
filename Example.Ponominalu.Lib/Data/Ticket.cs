﻿using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Data
{
	/// <summary>
	/// Билет
	/// </summary>
	public class Ticket
	{
		/// <summary>
		/// ID билетв
		/// </summary>
		[JsonProperty(PropertyName="id")]
		public int Id { get; set; }
		/// <summary>
		/// Цена билета
		/// </summary>
		[JsonProperty(PropertyName = "price")]
		public int Price { get; set; }
		/// <summary>
		/// ID события
		/// </summary>
		[JsonProperty(PropertyName = "event_id")]
		public int EventId { get; set; }
		/// <summary>
		/// ID сектора
		/// </summary>
		[JsonProperty(PropertyName = "sector_id")]
		public int SectorId { get; set; }
	}
}