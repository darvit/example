﻿using Newtonsoft.Json;

namespace Example.Ponominalu.Lib.Data
{
	/// <summary>
	/// Место в ряду
	/// </summary>
	public sealed class Seat
	{
		public Seat()
		{
			Id = 0;
			Number = 0;
			Reserved = true;
			Price = 0;
		}

		/// <summary>
		/// ID места
		/// </summary>
		/// <remarks>
		/// Если место занято, ID будет равен 0
		/// </remarks>
		[JsonProperty("id")]
		public int Id { get; set; }
		/// <summary>
		/// Номер места
		/// </summary>
		/// <remarks>
		/// Место с номером '0' является проходом
		/// </remarks>
		[JsonProperty("number")]
		public int Number { get; set; }
		/// <summary>
		/// Признак занятого места
		/// </summary>
		[JsonProperty("reserved")]
		public bool Reserved { get; set; }
		/// <summary>
		/// Цена билета на место
		/// </summary>
		/// <remarks>
		/// Если место занято, цена будет равна 0
		/// </remarks>
		[JsonProperty("price")]
		public int Price { get; set; }
	}
}