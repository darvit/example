﻿using System;

using Example.Ponominalu.Lib.Core.Services;

namespace Example.Ponominalu.Lib
{
	/// <summary>
	/// Фабрика сервисов событий
	/// </summary>
	public class EventsServiceFactory
	{
		/// <summary>
		/// Создание экземпляра сервиса
		/// </summary>
		/// <returns>Сервис событий</returns>
		public static IEventsService Create()
		{
			return Activator.CreateInstance<PonominaluEventsService>();
		}
	}
}