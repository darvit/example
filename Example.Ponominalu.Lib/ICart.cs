﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Example.Ponominalu.Lib.Data;

namespace Example.Ponominalu.Lib
{
	/// <summary>
	/// Интерфейс билетной корзины
	/// </summary>
	public interface ICart
	{
		/// <summary>
		/// Общая стоимость билетов в корзине
		/// </summary>
		int Amount { get; }
		/// <summary>
		/// Комиссия
		/// </summary>
		int Commission { get; }
		/// <summary>
		/// Список билетов в корзине
		/// </summary>
		List<Ticket> Tickets { get; }

		/// <summary>
		/// Добавить билет
		/// </summary>
		/// <param name="id">ID билета</param>
		/// <param name="eventId">ID события</param>
		/// <param name="sectorId">ID сектора</param>
		/// <param name="price">Цена билета</param>
		/// <returns>Статус билета: true - билет забронирован и добавлен в корзину</returns>
		Task<bool> AddTicket(int id, int eventId, int sectorId, int price);
		/// <summary>
		/// Доавить входные билеты
		/// </summary>
		/// <param name="eventId">ID события</param>
		/// <param name="sectorId">ID сектора</param>
		/// <param name="price">Цена билета</param>
		/// <param name="count">Колчество билетов</param>
		/// <returns>Статус билетов: true - билеты забронированы и добавлены в корзину</returns>
		Task<bool> AddAdmission(int eventId, int sectorId, int price, int count);
		/// <summary>
		/// Убрать билет из корзины
		/// </summary>
		/// <param name="ticket">ID билета</param>
		/// <returns>Статус билета: true - билет удален из корзиы</returns>
		Task<bool> RemoveTicket(int ticket);
		/// <summary>
		/// Очистка корзины
		/// </summary>
		/// <returns>true - все билеты удалены</returns>
		Task<bool> Clear();

		/// <summary>
		/// Создать заказ на основе корзины
		/// </summary>
		/// <returns></returns>
		IOrder CreateOrder();
	}
}