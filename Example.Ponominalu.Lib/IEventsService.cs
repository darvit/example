﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Example.Ponominalu.Lib.Data;

namespace Example.Ponominalu.Lib
{
	/// <summary>
	/// Сервис поиска мероприятий
	/// </summary>
	public interface IEventsService
	{
		/// <summary>
		/// Получение мероприятий из категории
		/// </summary>
		/// <param name="category">Категория</param>
		/// <returns>Список событий</returns>
		Task<List<EventPreview>> GetEventsFromCategory(ECategory category);
		/// <summary>
		/// Поиск событий
		/// </summary>
		/// <param name="queue">Запрос поиска</param>
		/// <returns>Мероприятия, удовлетворяющие критериям поиска</returns>
		Task<List<EventPreview>> SearchEvents(EventSearchQueue queue);
		/// <summary>
		/// Получение подробной информации о мероприятии по ID
		/// </summary>
		/// <param name="id">ID мероприятия</param>
		/// <returns>Расширенная информация</returns>
		Task<Event> GetEvent(int id);
	}
}